%.o: %.asm	
	@nasm -felf64 -o $@ $<
	
main.o: main.asm lib.asm dict.asm words.inc
	@nasm -felf64 -o $@ $<

program: main.o lib.o dict.o
	@ld -o $@ $^

clean: 
	@rm -f *.o
	@rm -f program
test:
	@python3 test.py

.PHONY: clean test
