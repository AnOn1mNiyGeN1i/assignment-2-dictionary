import subprocess
import os

inputs = ["", "first_word", "second_word", "third_word", "third"*256]
exp_out = ["", "first", "second", "third", ""]
exp_err = ["There is no such key in a dictionary", "", "", "", "Key error"]
errors=[]


wrong = 0;

for i in range(len(inputs)):
	process = subprocess.run("./program", input=inputs[i], text=True, capture_output=True)
	
	if process.stdout != exp_out[i] or process.stderr != exp_err[i]:
		print("------->", bool(process.stdout != exp_out[i]), bool(process.stderr != exp_err[i]))
		print(process.stdout, "exp out: ", exp_out[i], " err:", process.stderr, "exp err: ", exp_err[i])
		#print("Test failed")
		wrong += 1

if wrong == 0:
	print("All tests are passed")
else:
	print("WA count = ", wrong)

