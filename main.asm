%include "lib.inc"
%include "dict.inc"
%include "words.inc"
%define BUFF_SIZE 256

section .rodata
not_found: db "There is no such key in a dictionary", 0
key_size_err: db "Key error", 0
outp: db "debug out", 0

section .bss
buf: resb BUFF_SIZE

section .text

global _start


_start:
		mov rdi, buf
		mov rsi, BUFF_SIZE
		call read_string
		test rax, rax
		jz .end_if_long_key
		
		mov rdi, rax
		mov rsi, prev
		call find_word
		test rax, rax
		jz .end_if_key_not_found
		add rax, 8
		add rax, rdx
		inc rax
		mov rdi, rax
		call print_string
		jmp .end
	.end_if_long_key:
		mov rdi, key_size_err
		jmp .err
	.end_if_key_not_found:
		mov rdi, not_found
	.err:
		call print_error
	.end:
		call exit
